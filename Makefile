HUG = pipenv run hug -m bigbugs

run:
	$(HUG)


initdb:
	$(HUG) -c initdb


format:
	pipenv run black ./bigbugs
