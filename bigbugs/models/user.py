import re
from sqlalchemy import Column, String, Boolean
from sqlalchemy.orm import relationship
from passlib.hash import pbkdf2_sha256
import jwt
from . import Base, Session
from .. import errors
from .. import SECRET_KEY, JWT_ALGO


class User(Base):
    __tablename__ = "user"

    name = Column(String, unique=False, nullable=False)
    email = Column(String, unique=True, nullable=False)
    password = Column(String, nullable=False)
    phone_number = Column(String, nullable=False)
    id_type = Column(String, nullable=False)
    id_number = Column(String, nullable=False)
    next_of_kin = Column(String, nullable=False)
    deleted = Column(Boolean, nullable=False, default=False)
    accounts = relationship("Account", backref="user")

    def check_password(self, password: str) -> bool:
        """Verify the given password against the stored hash."""
        return pbkdf2_sha256.verify(password, self.password)

    def set_password(self, password: str, save: bool = False):
        """Hash and set the password on this instance."""
        if len(password) < 8:
            raise errors.InvalidPassword("Password must be at least 8 characters")

        hashed = pbkdf2_sha256.hash(password)
        self.password = hashed

        if save:
            Session().commit()

    def delete(self):
        """
        Soft delete the user. We just set a flag instead of deleting the row to
        make sure we don't break any foreign keys. This is not GDPR compliant,
        we're not in Europe.
        """
        self.deleted = True
        Session().commit()

    def create_user(self) -> str:
        """
        Validate the data stored in the instance, and create the user if
        everything checks out.

        :returns: The id of the newly created user
        :rtype: str
        """
        session = Session()

        existing = User.get(self.email)
        if existing and existing.deleted:
            raise errors.DeletedUser
        elif existing:
            raise errors.AccountExists

        if not re.match("\\d" * 10, self.phone_number):
            raise errors.InvalidPhoneNumber

        # TODO add support for oauth
        if not self.password:
            raise ValueError("no password stored")

        session.add(self)
        session.commit()

        return self.id

    @classmethod
    def get(cls, email: str):
        """Fetch a user by the given email address."""
        return Session().query(cls).filter_by(email=email).one_or_none()

    @classmethod
    def basic_user_loader(cls, email: str, password: str):
        "For use by falcon-auth"
        session = Session()
        user = self.get(email)

        if not user:
            raise errors.UserNotFound

        if not user.check_password(password):
            raise errors.InvalidPassword

        if user.deleted:
            raise errors.UserNotFound

        return user

    @classmethod
    def token_user_loader(cls, payload):
        "For use by falcon-auth"
        email = payload["user"]["email"]

        user = cls.get(email)

        if user.deleted:
            raise errors.UserNotFound

        return user

    def json(self):
        return {
            "user_id": self.id,
            "email": self.email,
            "phone_number": self.phone_number,
            "id_type": self.id_type,
            "id_number": self.id_number,
            "next_of_kin": self.next_of_kin,
        }
