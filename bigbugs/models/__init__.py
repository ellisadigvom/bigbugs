import os
import uuid
from datetime import datetime
from sqlalchemy import create_engine, func, Column, DateTime, CHAR
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker, scoped_session


def uuid4():
    """The function we use to generate a value for the id column"""
    return str(uuid.uuid4())


class Base:
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(CHAR(36), primary_key=True, default=uuid4)
    created_at = Column(DateTime(timezone=True), default=func.now(), nullable=False)
    updated_at = Column(DateTime(timezone=True), default=None, onupdate=datetime.now)


Base = declarative_base(cls=Base)


_engine = None


def get_engine():
    global _engine
    url = os.getenv("DATABASE_URL")
    if not _engine:
        _engine = create_engine(url)
    return _engine


def set_engine(engine):
    _engine = engine
    Session.configure(bind=engine)


session_factory = sessionmaker(bind=get_engine())
# scoped so we only create one instance per thread
Session = scoped_session(session_factory)


from .user import User
from .token import Token
from .account import Account
from .transaction import Transaction
