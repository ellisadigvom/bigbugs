from sqlalchemy import Column, String, Float, ForeignKey
from . import Base, Session
from .. import errors


class Transaction(Base):
    __tablename__ = "transaction"

    kind = Column(String, unique=False, nullable=False)
    user_id = Column(String, ForeignKey("user.id"), unique=False, nullable=False)
    account_id = Column(String, ForeignKey("account.id"), unique=False, nullable=False)
    amount = Column(Float, unique=False, nullable=False, default=0)

    def json(self):
        return {
            "kind": self.kind,
            "amount": self.amount,
            "created_at": self.created_at,
            "account_id": self.account_id,
        }
