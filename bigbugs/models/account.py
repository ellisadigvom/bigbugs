from sqlalchemy import Column, String, Float, ForeignKey
from sqlalchemy.orm import relationship
from . import Base, Session
from .. import errors


class Account(Base):
    __tablename__ = "account"

    name = Column(String, unique=False, nullable=False)
    user_id = Column(String, ForeignKey("user.id"), unique=False, nullable=False)
    balance = Column(Float, unique=False, nullable=False, default=0)
    transactions = relationship("Transaction", backref="account")

    def json(self):
        return {
            "name": self.name,
            "balance": self.balance,
            "created_at": self.created_at,
            "transactions": [t.json() for t in self.transactions],
        }
