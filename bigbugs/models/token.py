from datetime import timedelta, datetime
from sqlalchemy import Column, String
import jwt
from . import Base, Session
from .. import errors
from .. import SECRET_KEY, JWT_ALGO


class Token(Base):
    __tablename__ = "used_token"

    token = Column(String, unique=False, nullable=False)

    @classmethod
    def consume(cls, token: str):
        """Mark a one time token as used"""
        t = cls(token=token)
        s = Session()
        s.add(t)
        s.commit()

    @classmethod
    def is_used(cls, token: str) -> bool:
        """Check if the given token has been used"""
        t = Session().query(cls).filter_by(token=token).first()
        return bool(t)

    @classmethod
    def generate(cls, payload: dict) -> str:
        """Generate a JWT token with the given data"""
        return jwt.encode(payload, SECRET_KEY, algorithm=JWT_ALGO).decode()

    @classmethod
    def generate_password_reset(cls, email: str) -> str:
        """Generate a password reset token for the user with the given email"""
        payload = {
            "type": "PASSWORD_RESET",
            "email": email,
            "exp": datetime.utcnow() + timedelta(days=1),
        }
        return cls.generate(payload)

    @classmethod
    def validate_password_reset(cls, token: str) -> str:
        """
        Validate the given password reset token

        :returns: The email of the user to which the token belongs
        :raises: errors.InvalidToken
        """
        try:
            payload = jwt.decode(token, SECRET_KEY, algorithms=[JWT_ALGO])
        except jwt.exceptions.ExpiredSignatureError as e:
            raise errors.InvalidToken

        if payload["type"] != "PASSWORD_RESET":
            raise errors.InvalidToken

        if cls.is_used(token):
            raise errors.InvalidToken

        return payload["email"]
