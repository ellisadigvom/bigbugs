from datetime import datetime, timedelta
from random import randrange
from . import models


def insert(user_id: str):
    s = models.Session()

    stocks = models.Account(user_id=user_id, name="Assorted Stocks", balance=500)
    s.add(stocks)
    s.commit()

    for i in range(100):
        created_at = datetime.now() - timedelta(days=i * 7)
        tx = models.Transaction(
            kind={0: "DEPOSIT", 1: "WITHDRAWAL", 2: "INTEREST"}[randrange(3)],
            amount=randrange(500),
            account_id=stocks.id,
            created_at=created_at,
            user_id=user_id,
        )
        s.add(tx)

    s.commit()
