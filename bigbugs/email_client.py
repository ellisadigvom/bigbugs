import os
from logging import getLogger

# TODO might want to use a nicer library for this
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
import smtplib


log = getLogger(__name__)


class EmailClient:
    def send(self, to_addr: str, subject: str, body: str):
        """Send an email"""
        msg = MIMEMultipart()
        msg["From"] = os.getenv("GMAIL_ADDRESS")
        msg["To"] = to_addr
        msg["Subject"] = subject
        msg.attach(MIMEText(body, "plain"))

        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()
        server.login(os.getenv("GMAIL_ADDRESS"), os.getenv("GMAIL_PASSWORD"))

        server.sendmail(os.getenv("GMAIL_ADDRESS"), to_addr, msg.as_string())

    def password_reset(self, email: str, token: str):
        """Send an email to the user with their password reset token"""
        self.send(
            email,
            "BigBugs password reset",
            "Your password reset token is {}".format(token),
        )
