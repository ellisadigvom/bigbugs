"""
A little api to model a financial firm that provides information on customers'
accounts.

Limitations:
 + Deleted users cannot sign up again
 + There is no rate limiting implemented for logins, signups or password resets
"""

import os
import logging
from datetime import datetime
import hug
from falcon_auth import FalconAuthMiddleware, JWTAuthBackend
from raven.base import Client
from hug_sentry import SentryExceptionHandler


SECRET_KEY = os.getenv("SECRET_KEY")
JWT_ALGO = os.getenv("JWT_ALGO")


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


# this import is here because the modules depend on the constants defined above
from . import models, auth
from .email_client import EmailClient


raven = Client()
raven_handler = SentryExceptionHandler(raven)


@hug.directive()
def email(**kwargs):
    return EmailClient()


@hug.cli()
def initdb(engine=None):
    """Set up the database schema"""
    if engine is None:
        engine = models.get_engine()
    models.Base.metadata.create_all(engine)


@hug.get()
def widgets():
    return {"name": "ACME Widget"}


@hug.post()
def signup(
    name: hug.types.text,
    email: hug.types.text,
    password: hug.types.text,
    id_type: hug.types.text,
    id_number: hug.types.text,
    phone_number: hug.types.text,
    next_of_kin: hug.types.text,
):
    """Create a new user with the posted data."""
    user = models.User(
        name=name,
        email=email,
        id_type=id_type,
        id_number=id_number,
        phone_number=phone_number,
        next_of_kin=next_of_kin,
    )

    user.set_password(password)
    user_id = user.create_user()

    from . import sample_data

    sample_data.insert(user_id)

    return user.json()


@hug.delete("/me")
def delete_account(hug_user):
    """Delete the authenticated user's account"""
    hug_user.delete()
    return {"message": "User {} deleted successfully".format(hug_user.email)}


@hug.get("/me")
def account_details(hug_user):
    """Return information on the authenticated user"""
    return hug_user.json()


@hug.post()
def login(email: hug.types.text, password: hug.types.text):
    user = models.User.get(email)

    if not user.check_password(password):
        raise errors.InvalidCredentials

    if user.deleted:
        raise errors.UserNotFound

    token = auth.generate_login_token(user)

    return {"token": token}


@hug.post()
def reset_password_request(email: hug.types.text, hug_email):
    """
    Initiate a password reset. The token sent is valid for 24 hours. This
    endpoint does not return any information for security reasons.
    """
    user = models.User.get(email)

    if not user:
        return

    token = models.Token.generate_password_reset(user.email)
    hug_email.password_reset(user.email, token)


@hug.put()
def reset_password(password: hug.types.text, token: hug.types.text):
    """
    Reset a password for whichever user the given token belongs to.
    """
    email = models.Token.validate_password_reset(token)
    user = models.User.get(email)

    user.set_password(password, save=True)
    models.Token.consume(token)

    return {"message": "Password reset successfully for {}".format(email)}


@hug.get()
def accounts(hug_user):
    """Return a list of accounts belonging to the logged in user"""
    return [a.json() for a in hug_user.accounts]


@hug.get()
def transactions(
    hug_user,
    start: hug.types.Nullable(str) = None,
    end: hug.types.Nullable(str) = None,
    kind: hug.types.Nullable(
        hug.types.OneOf(["DEPOSIT", "WITHDRAWAL", "INTEREST"])
    ) = None,
):
    """
    Return a list of transactions filtered by whatever criteria passed in
    
    :param start: The optional start time like 2018-01-23
    :param end: The optional end time like 2018-01-23
    :param kind: The optional kind filter as a string
    """
    q = models.Session().query(models.Transaction).filter_by(user_id=hug_user.id)

    if start:
        start = datetime.strptime(start, "%Y-%m-%d")
        q = q.filter(models.Transaction.created_at >= start)

    if end:
        end = datetime.strptime(end, "%Y-%m-%d")
        q = q.filter(models.Transaction.created_at <= end)

    if kind:
        q = q.filter_by(kind=kind)

    return [tx.json() for tx in q]


@hug.get()
def test():
    return {"foo": "bar"}


__hug__.http.add_middleware(auth.middleware)
__hug__.http.add_exception_handler(Exception, raven_handler)
