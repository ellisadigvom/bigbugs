from falcon import HTTPError
import falcon.status_codes as status


class AccountExists(HTTPError):
    def __init__(self):
        super().__init__(status.HTTP_400, title="Account already exists")


class InvalidPassword(HTTPError):
    def __init__(self, description):
        super().__init__(
            status.HTTP_400, title="Invalid password", description=description
        )


class InvalidCredentials(HTTPError):
    def __init__(self):
        super().__init__(status.HTTP_401, title="Invalid credentials")


class UserNotFound(InvalidCredentials):
    pass


class DeletedUser(HTTPError):
    def __init__(self):
        super().__init__(
            status.HTTP_401, title="The user with that email has been deleted"
        )


class InvalidToken(HTTPError):
    def __init__(self):
        super().__init__(status.HTTP_401, title="The supplied token is invalid")


class InvalidPhoneNumber(HTTPError):
    def __init__(self):
        super().__init__(
            status.HTTP_400, title="Please enter a phone number in local Ghanian format"
        )
