from falcon_auth import FalconAuthMiddleware, BasicAuthBackend, JWTAuthBackend
import jwt
from . import models, SECRET_KEY, JWT_ALGO


token_auth_backend = JWTAuthBackend(
    user_loader=models.User.token_user_loader,
    secret_key=SECRET_KEY,
    auth_header_prefix="Bearer",
)
middleware = FalconAuthMiddleware(
    token_auth_backend,
    exempt_routes=["/login", "/signup", "/reset_password", "/reset_password_request"],
)


def generate_login_token(user: models.User) -> str:
    return token_auth_backend.get_auth_token(user.json())
