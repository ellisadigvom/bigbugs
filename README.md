# Requirements
 + Postgresql
 + Python >= 3.6
 + pipenv


# Development setup
 + Run the command `pipenv install` in this directory to install dependencies
 + Edit the .env file to include the proper db credentials, and API keys. You
   may do this my making a copy of `.env.sample` and adding the necessary
   values.
 + Run `make initdb` to setup the database schema

Please note that these instructions are not sufficient for running this app in
production. You'll want to run it under gunicorn or some other WSGI server.

Why you may want to run this in production, I've no idea, but this warning is
here just in case.


# Running
To start the application run the command `make run` in this directory.


# Heroku
Alternatively, this app may be run under Heroku. In this case, the
configuration found in `.env.sample` has to be replicated in Heroku.
Once that's done, the `Procfile` in this repository is sufficient to run the
application.


# Documentation
## Sign Up
Accounts can be created by making a POST request to the `/signup` endpoint with
the following information.

``` json
{
  "name": "John Doe",
  "email": "foo@bar.com",
  "password": "hackmeplease",
  "id_type": "NATIONAL_ID",
  "id_number": "ASDF1234",
  "phone_number": "0212345678",
  "next_of_kin": "Jane Doe"
}
```

The phone number must be in the Ghanaian local format. On successful account
creation, the endpoint will return the user object, along with it's id in the
system.


## Log In
Authentication is handled in a RESTful manner, by passing a token to the server
with each request. The token can be obtained by making a POST to the `/login`
endpoint with the email and password.

``` json
{
  "email": "foo@bar.com",
  "password": "hackmeplease"
}
```

This token should be passed to the server in the Authentication http header as
a bearer token. The relevant section of the raw HTTP request will look as
follows:

```
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
```


## Authenticated user details
Information on the currently authenticated user (i.e. the user to which the
passed token belongs) can be accessed by making a GET request to the `/me`
endpoint.


## User deletion
A user login can be deleted by making an authenticated DELETE request to the
`/me` endpoint. Note that there is currently no way to sign again after account
deletion.


## Financial account details
Where account is synonymous with bank account. The user's accounts can be
retrieved by making an authenticated GET request to the `/accounts` endpoint.
This will return a list of account objects, their balances and all their
transactions.


## Transaction retrieval
The user's transactions can be fetched by making a GET request to the
`/transactions` endpoint. Transactions can be limited to a certain time period
by passing `start` and `end` parameters as part of the query string. The dates
must follow the format `%Y-%m-%d`.

Additionally transactions can be limited to deposits, withdrawals or interest
payments by passing the corresponding types in the `kind` query parameter. This
parameter can take the following values:

 + `DEPOSIT`
 + `WITHDRAWAL`
 + `INTEREST`


## Password reset
To initiate a password reset request, make a POST request to
`/reset_password_request` with the email address. This endpoint will not return
any status information for security reasons.

``` json
{
  "email": "foo@bar.com"
}
```

Once that's done, if the address exists, a token will be sent to it. To change
the user's password, make a PUT request to the `/reset_password` endpoint with
the token and the new password.

``` json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlIjoiUEFTU1dPUkRfUkVTRVQiLCJlbWFpbCI6ImZvb0BiYXIuY29tIiwiZXhwIjoxNTM0NTEyNDc4fQ.qVAyHBKEW36-Z4kbwCYG22EQTOjvhf4O_k7dHVZGhuo",
  "password": "srsly, hack me"
}
```

# Tasks
## Wrap server response in an object
## Allow limiting fetched transactions to a specific account
